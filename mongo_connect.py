"""
require pymongo, pandas
"""

from pymongo import MongoClient
import pandas as pd 
import logging

class Mongo():
    client = None
    con = None

    def __init__(self, data):
        if data.get("username") is None:
            self.client = MongoClient(host=data["host"], port=data["port"])
        else:
            self.client = MongoClient(host=data["host"], port=data["port"], username=data["username"], password=data["password"],
                    authSource=data["auth"])
        self.con = self.client[data["db"]]

    def __clear(self, data):
        for d in data:
            keys = list(d.keys())
            for k in keys:
                if type(d[k]) in (list, dict, set):
                    continue
                elif pd.isnull(d[k]):
                    d.pop(k)
        return data

    def read(self, col, cond, *, sort=None, limit=None, skip=None, ext={}):
        ext.update({"_id":0})
        cursor = self.con[col].find(cond, ext)
        if sort is not None:
            cursor.sort(sort)
        if limit is not None:
            cursor.limit(limit)
        if skip is not None:
            cursor.skip(skip)
        return list(cursor)

    def read_df(self, col, cond, *, sort=None, limit=None, skip=None, ext={}):
        df = pd.DataFrame(self.read(col, cond, sort=sort, limit=limit, skip=skip, ext=ext))
        return df

    def write(self, col, data, *, clear=False):
        if len(data)==0:
            return 
        elif type(data)==dict:
            self.con[col].insert_one(data)
        else:
            if clear:
                data = self.__clear(data)
            self.con[col].insert_many(data)

    def write_df(self, col, df):
        if df.shape[0]==0:
            return
        data = df.to_dict("records")
        data = self.__clear(data)
        self.write(col, data)
        #self.con[col].insert_many(df.to_dict("records"))

    def remove(self, col, cond, *, one=False):
        if one:
            self.con[col].delete_one(cond)
        else:
            self.con[col].delete_many(cond)

"""
if __name__ == '__main__':
    #df = Mongo().read('PRI', {"date":"20220413"}, sort=[('number',-1)])
    #df = Mongo().read('PRI', {"date":"20220413"}, limit=1)
    df = Mongo().read('PRI', {"date":"20220413"}, limit=1, skip=1)
    for d in df.iterrows():
        print(d)
"""
