"""

"""

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging

class Gmail():
    FROM = ''
    TO = ''
    smtp = None
    def __init__(self, account, key, *, to=None):
        try:
            self.FROM = account
            smtp = smtplib.SMTP('smtp.gmail.com', 587)
            smtp.starttls()  # 建立加密傳輸
            smtp.login(account, key)  # 登入寄件者gmail
            self.smtp = smtp
            if to is not None:            
                self.TO = to
        except Exception as e:
            logging.error("Error message: ", e)


    def send(self, subject, text, *, to=None):
        content = MIMEMultipart()  #建立MIMEMultipart物件
        content["subject"] = subject
        content["from"] = self.FROM
        if to is None:
            content['to'] = self.TO
        else:
            content["to"] = to
        content.attach(MIMEText(text))  #郵件內容
        try:
            self.smtp.send_message(content)  # 寄送郵件
        except Exception as e:
            logging.error("send mail fail: ", e)

    
